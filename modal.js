/* Improved version of modal directive. 
 * Every instance gets it's own scope via inheritance
 *
 */

angular.module("modal", [])
	.directive("myModal", function() {
		return {
			// allow only to be used as element
			restrict: "E",
			
			// allow contents of the element to be wrapped by template
			transclude: true,
			
			// use external template
			templateUrl: "app/components/modal.html",
			
			// create own scope inheriting from parent scope
			scope: true,
			
			// bind open/close to variable, given in my-modal-toggle-variable attribute
			link: function(scope, element, attr) {
				
				// check that an atribute has been given
				if(attr["myModalToggleVariable"]) {
					
					// Set internaly scoped function to check for open/close state:
					// Note pattern of using self-executing anonymous function 
					// to produce a function which will be used later. Here
					// we make a closure over the given variable in parent scope.
					scope["getToggleState"] = (function(v) {
						return function() {
							return angular.isDefined(scope.$parent[v]) ? scope.$parent[v] : false;
						}
					})(attr["myModalToggleVariable"]);
					
					// Set internally scoped function, which is binded to close button click.
					// It toggles a variable in the parent's scope.
					scope["closeModal"] = (function(v) {
						return function() {
							scope.$parent[v] = false;
						}
					})(attr["myModalToggleVariable"]);
				}
			}
		};
	});